# Translation of docs_kdenlive_org_more_information___earlier_versions___Configure_capture_firewire.po to Catalan
# Copyright (C) 2025 This_file_is_part_of_KDE
# Licensed under the <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2025.
msgid ""
msgstr ""
"Project-Id-Version: documentation-docs-kdenlive-org\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2025-01-14 01:50+0000\n"
"PO-Revision-Date: 2025-01-20 12:32+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.3\n"

#: ../../more_information/earlier_versions/Configure_capture_firewire.rst:1
msgid "Kdenlive Documentation - Configuration Capture Firewire"
msgstr "Documentació del Kdenlive - Configuració de la captura del Firewire"

#: ../../more_information/earlier_versions/Configure_capture_firewire.rst:1
msgid ""
"KDE, Kdenlive, documentation, user manual, configuration, preferences, "
"capture, firewire, video capture, video editor, open source, free, learn, "
"easy"
msgstr ""
"KDE, Kdenlive, documentació, manual d'usuari, configuració, preferències, "
"captura, firewire, captura de vídeo, editor de vídeo, codi lliure, lliure, "
"aprendre, fàcil"

#: ../../more_information/earlier_versions/Configure_capture_firewire.rst:22
msgid ""
"This page is not maintained anymore and contains information referring to "
"features or functions from earlier versions of Kdenlive that are deprecated "
"or have been superseded by something else."
msgstr ""
"Aquesta pàgina ja no es manté i conté informació referida a característiques "
"o funcions de versions anteriors del Kdenlive que estan en desús o han estat "
"reemplaçades per alguna altra cosa."

#: ../../more_information/earlier_versions/Configure_capture_firewire.rst:26
msgid "Configure Firewire Capture"
msgstr "Configuració de la captura del Firewire"

#: ../../more_information/earlier_versions/Configure_capture_firewire.rst:28
msgid ""
"The image shows the Configure Firewire capture tab which can be accessed "
"from the :menuselection:`Settings --> Configure Kdenlive` menu or from the "
"spanner icon in the capturing."
msgstr ""
"La imatge mostra la pestanya de Configuració de la captura del Firewire que "
"és accessible des del menú :menuselection:`Arranjament --> Configura el "
"Kdenlive` o des de la icona de l'expansor a la captura."

# skip-rule: t-acc_obe
#: ../../more_information/earlier_versions/Configure_capture_firewire.rst:37
msgid ""
"The firewire capture functionality uses the `dvgrab <http://linux.die.net/"
"man/1/dvgrab>`_ program. The settings applied here to define how dvgrab will "
"be used to capture the video."
msgstr ""
"La funcionalitat de captura del Firewire usa el programa `dvgrab <http://"
"linux.die.net/man/1/dvgrab>`_. Els paràmetres aplicats aquí defineixen com "
"s'usarà el «dvgrab» per a capturar el vídeo."

#: ../../more_information/earlier_versions/Configure_capture_firewire.rst:40
msgid "**Capture Format options** are"
msgstr "Les **opcions de format de captura** són"

#: ../../more_information/earlier_versions/Configure_capture_firewire.rst:42
msgid "DV RAW"
msgstr "DV RAW"

#: ../../more_information/earlier_versions/Configure_capture_firewire.rst:43
msgid "DV AVI Type 1"
msgstr "DV AVI tipus 1"

#: ../../more_information/earlier_versions/Configure_capture_firewire.rst:44
msgid "DV AVI Type 2"
msgstr "DV AVI tipus 2"

#: ../../more_information/earlier_versions/Configure_capture_firewire.rst:45
msgid "HDV"
msgstr "HDV"

#: ../../more_information/earlier_versions/Configure_capture_firewire.rst:47
msgid ""
"The first three are quality-wise the same (exactly the same DV 25Mb/s "
"standard definition codec), just packed differently into the file. Type 2 "
"seems to be the most widely supported by other applications."
msgstr ""
"Les tres primeres tenen la mateixa qualitat (exactament el mateix còdec de "
"definició estàndard DV 25Mb/s), encara que amb diferent empaquetatge en el "
"fitxer. El tipus 2 sembla que és el més acceptat per altres aplicacions."

#: ../../more_information/earlier_versions/Configure_capture_firewire.rst:49
msgid ""
"The raw format contains just the plain video frames (with audio interleaved) "
"without any additional information. Raw is useful for some Linux software. "
"Files in this format can also be played with Windows QuickTime when renamed "
"to :file:`file.dv`."
msgstr ""
"El format RAW només conté els fotogrames de vídeo net (amb àudio "
"entrellaçat) sense cap informació addicional. El RAW és útil per a algun "
"programari Linux. Els fitxers en aquest format també es poden reproduir amb "
"el Windows QuickTime quan es reanomena a :file:`fitxer.dv`."

#: ../../more_information/earlier_versions/Configure_capture_firewire.rst:51
msgid ""
"AVI files may contain multiple streams. Typically, they include one video "
"and one audio stream. The native DV stream format already includes the audio "
"interleaved into its video stream. A type 1 DV AVI file only includes one DV "
"video stream where the audio must be extracted from the DV video stream. A "
"type 2 DV AVI file includes a separate audio stream in addition to the audio "
"data already interleaved in the DV video stream. Therefore, the type 2 DV "
"AVI file is redundant and consumes more space."
msgstr ""
"Els fitxers AVI poden contenir diversos fluxos. Normalment, inclouen un flux "
"de vídeo i un d'àudio. El format de flux DV natiu ja inclou l'àudio "
"entrellaçat en el seu flux de vídeo. Un fitxer AVI DV de tipus 1 només "
"inclou un flux de vídeo DV a on l'àudio s'ha d'extreure del flux de vídeo "
"DV. Un fitxer AVI DV de tipus 2 inclou un flux d'àudio separat "
"addicionalment a les dades ja entrellaçades en el flux de vídeo DV. Per "
"tant, el fitxer AVI DV de tipus 2 és redundant i ocupa més espai."

#: ../../more_information/earlier_versions/Configure_capture_firewire.rst:53
msgid "HDV is a high-definition format used on tape-based HD camcorders."
msgstr ""
"L'HDV és un format d'alta definició usat en càmeres de vídeo HD de cinta."

#: ../../more_information/earlier_versions/Configure_capture_firewire.rst:55
msgid ""
"**Add recording time to captured file name** option: If this is unchecked "
"then each captured file will get a sequential number post-pended to the file "
"names listed in the Capture file name setting. With this checked, date and "
"timestamp (derived from when the footage was captured) is post-pended to the "
"capture file name, e.g. **capture2012.07.15_11-38-37.dv**"
msgstr ""
"Opció **Afegeix l'hora d'enregistrament al nom de fitxer capturat**: Si està "
"desmarcada, cada fitxer capturat tindrà un número seqüencial afegit al nom "
"del fitxer llistat al paràmetre de nom de fitxer de captura. Quan està "
"marcada, la data i l'hora (derivada de quan es va capturar el metratge) "
"s'afegeix al nom de fitxer de captura, p. ex. **captura2012.07.15_11-38-37."
"dv**"

# skip-rule: t-acc_obe
#: ../../more_information/earlier_versions/Configure_capture_firewire.rst:57
msgid ""
"**Automatically start a new file on scene cut** option:  With this checked "
"it tries to detect whenever a new recording starts, and store it into a "
"separate file. This is the -autosplit parameter in  `dvgrab <http://linux."
"die.net/man/1/dvgrab>`_  and it works by detecting timecode discontinuities "
"from the source footage.  Where a timecode discontinuity is anything "
"backward or greater than one second it will start a new capture file."
msgstr ""
"Opció **Inicia automàticament un fitxer nou en retallar una escena**:  Amb "
"això marcat, s'intenta detectar si comença un enregistrament nou, i "
"l'emmagatzema en un fitxer separat. Aquest és el paràmetre «-autosplit» del "
"`dvgrab <http://linux.die.net/man/1/dvgrab>`_  i funciona detectant "
"discontinuïtats del codi de temps en el metratge original. Quan la "
"discontinuïtat del codi de temps és cap enrere o superior a un segon, "
"comença un fitxer de captura nou."

# skip-rule: t-acc_obe
#: ../../more_information/earlier_versions/Configure_capture_firewire.rst:59
msgid ""
"The **dvgrab additional parameters** edit box allows you to add extra dvgrab "
"switches to the capture process that will run. See  `dvgrab manual <http://"
"linux.die.net/man/1/dvgrab>`_ for more info."
msgstr ""
"El quadre d'edició de **paràmetres addicionals del «dvgrab»** permet afegir "
"commutadors extres del «dvgrab» al procés de captura que s'executarà. Vegeu "
"el `manual del «dvgrab» <http://linux.die.net/man/1/dvgrab>`_ per a més "
"informació."
