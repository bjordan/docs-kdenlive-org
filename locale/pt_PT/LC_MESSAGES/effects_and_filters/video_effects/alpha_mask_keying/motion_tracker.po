# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-12-05 01:46+0000\n"
"PO-Revision-Date: 2023-06-11 02:14+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: OpenCV Open Source Vision Library application\n"
"X-POFile-SpellExtra: menuselection ref exchangekeyframesacrosseffects KCF\n"
"X-POFile-SpellExtra: CSRT Discriminative Correlation Filter with Channel\n"
"X-POFile-SpellExtra: and Spatial Reliability DCF CSR HoGs Colornames IPS\n"
"X-POFile-SpellExtra: ips MedianFlow DaSiam DaSiamRPN deep learning Flatpak\n"
"X-POFile-SpellExtra: AppData kdenlive opencvmodels kbd Win Kdenlive clip\n"
"X-POFile-SpellExtra: guilabel exchangekeyframesacrosseffectsA edit mode\n"

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:0
msgid "**Status**"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:0
msgid "**Keyframes**"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:0
msgid "**Source library**"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:0
msgid "**Source filter**"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:0
msgid "**Available**"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:0
msgid "**On Master only**"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:0
msgid "**Known bugs**"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:0
#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:105
msgid "KCF"
msgstr "KCF"

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:0
#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:107
msgid "CSRT"
msgstr "CSRT"

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:0
#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:109
msgid "MOSSE"
msgstr "MOSSE"

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:0
#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:111
msgid "MIL"
msgstr "MIL"

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:0
#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:113
msgid "MedianFlow"
msgstr "MedianFlow"

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:0
#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:115
msgid "DaSiam"
msgstr "DaSiam"

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:0
#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:117
msgid "Nano"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:0
#, fuzzy
#| msgid "**Linux**"
msgid "Linux"
msgstr "**Linux**"

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:0
msgid "Flatpak"
msgstr "Flatpak"

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:0
msgid "MacOS"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:0
#, fuzzy
#| msgid "**Windows**"
msgid "Windows"
msgstr "**Windows**"

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:1
msgid "Kdenlive Video Effects - Motion Tracker"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:1
msgid ""
"KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video "
"effects, motion, tracking, motion tracker"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:29
msgid "Motion Tracker"
msgstr "Seguimento do Movimento"

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:41
msgid "Maintained"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:43
msgid "Yes"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:45
msgid "opencv"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:47
#, fuzzy
#| msgid "Motion Tracker"
msgid "tracker"
msgstr "Seguimento do Movimento"

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:49
msgid "|linux| |appimage| |windows| |apple|"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:51
#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:53
msgid "No"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:59
#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:73
msgid "Description"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:60
#, fuzzy
#| msgid ""
#| "Motion tracking is the process of locating a moving object across time. "
#| "Kdenlive uses |OpenCV| for motion detection. The results of this effect "
#| "can be used in other effects by copying the keyframe data generated by "
#| "the Motion Tracker as position keyframes in the Transform effect, for "
#| "example."
msgid ""
"Motion tracking is the process of locating a moving object across time. "
"Kdenlive uses |OpenCV|\\ [1]_ for motion detection. The results of this "
"effect can be used in other effects by copying the keyframe data generated "
"by the Motion Tracker as position keyframes in the Transform effect, for "
"example."
msgstr ""
"O seguimento do movimento é o processo de localizar um objecto em movimento "
"ao longo do tempo. O Kdenlive usa o |OpenCV| para a detecção de movimento. O "
"resultado desse efeito pode ser usado noutros efeitos, copiando os dados da "
"imagem-chave gerados pelo Seguimento do Movimento, como as imagens-chave da "
"posição no efeito de Transformação, por exemplo."

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:64
msgid "Parameters"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:71
msgid "Parameter"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:72
msgid "Value"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:74
#, fuzzy
#| msgid "Tracking algorithms"
msgid "Tracker algorithm"
msgstr "Algoritmos de seguimento"

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:75
#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:81
#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:93
msgid "Selection"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:76
msgid "Sets the algorithm used for motion tracking"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:77
msgid "Keyframes spacing"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:78
#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:84
#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:90
msgid "Integer"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:79
msgid "Determines how many keyframes can be skipped when analyzing"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:80
msgid "Frame shape"
msgstr "Forma do contorno"

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:82
#, fuzzy
#| msgid "Tracking the face of the model"
msgid "Set the shape of the frame"
msgstr "Acompanhar a cara do modelo"

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:83
msgid "Shape width"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:85
msgid "Set the thickness of the frame shape"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:86
msgid "Shape color"
msgstr "Cor da forma"

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:87
msgid "Picker"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:88
msgid ""
"Set the color of the frame shape. Also determines the color for :guilabel:"
"`Blur type` **Opaque Fill**."
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:89
#, fuzzy
#| msgid "Blur type"
msgid "Blur"
msgstr "Tipo de borrão"

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:91
msgid ""
"Set the amount of blur for :guilabel:`Blur type` **Median Blur** and "
"**Gaussian Blur**"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:92
msgid "Blur type"
msgstr "Tipo de borrão"

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:94
msgid "Select what to do with the framed section"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:96
msgid "The following selection items are available:"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:98
#, fuzzy
#| msgid "Tracking algorithms"
msgid ":guilabel:`Tracker algorithm`"
msgstr "Algoritmos de seguimento"

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:106
#, fuzzy
#| msgid "**Kernelized Correlation Filters**"
msgid "Kernelized Correlation Filters (default)"
msgstr "**Filtros de Correlação de Núcleos**"

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:108
msgid "Channel and Spatial Reliability Tracking"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:110
#, fuzzy
#| msgid "**Minimum Output Sum of Squared Error**"
msgid "Minimum Output Sum of Squared Error"
msgstr "**Resultado Mínimo da Soma dos Erros ao Quadrado**"

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:112
msgid "Multiple Instance Learning"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:116
#, fuzzy
#| msgid ""
#| "The |DaSiamRPN| visual tracking algorithm relies on deep-learning models "
#| "to provide extremely accurate results."
msgid ""
"The |DaSiamRPN| visual tracking algorithm relies on deep-learning models to "
"provide extremely accurate results. Please see note below for installation "
"instructions."
msgstr ""
"O algoritmo de seguimento visual |DaSiamRPN| baseia-se e algoritmos de 'deep "
"learning' para oferecer resultados extremamente precisos."

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:118
#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:253
msgid "Nano tracker is a lightweight model and gives good results and is fast."
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:120
msgid ""
"You may need to experiment with different tracking algorithms to produce "
"good results for your specific use case. See a short comparison of the "
"different `tracking algorithms`_ below."
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:122
msgid ":guilabel:`Frame Shape`"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:129
msgid "Rectangle"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:130
msgid "Default"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:131
msgid "Ellipse"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:133
msgid "Arrow"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:136
#, fuzzy
#| msgid ""
#| "Select from Rectangle (default), Ellipse or Arrow. Choosing the right "
#| "shape can make the tracking better."
msgid "Selecting the right shape type can make the motion tracking better."
msgstr ""
"Seleccionar no Rectângulo (por omissão), Elipse ou Seta. A escolha da forma "
"correcta poderá tornar o seguimento melhor."

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:138
msgid ":guilabel:`Blur Type`"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:145
msgid "None"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:146
msgid "Do nothing (default)"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:147
#, fuzzy
#| msgid "MedianFlow"
msgid "Median Blur"
msgstr "MedianFlow"

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:148
msgid "Apply median blur to rectangle"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:149
msgid "Gaussian Blur"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:150
msgid "Apply Gaussian blur to rectangle"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:151
msgid "Pixelate"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:152
msgid "Pixelate rectangle"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:153
msgid "Opaque fill"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:154
msgid "Fill rectangle with shape color"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:156
msgid "Examples for :guilabel:`Blur Type`:"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:162
msgid "Different blur types in action"
msgstr "Tipos de borrão diferentes em acção"

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:166
#, fuzzy
#| msgid "How to track a region of a video?"
msgid "How to Track a Region of a Video"
msgstr "Como seguir uma região de um vídeo?"

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:168
#, fuzzy
#| msgid "The basic workflow for tracking a region is:"
msgid "The basic workflow for tracking a region is as follows:"
msgstr "O processo básico para acompanhar uma região consiste em:"

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:177
msgid "Tracking the face of the model"
msgstr "Acompanhar a cara do modelo"

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:179
msgid "Apply the effect to a clip"
msgstr "Aplicar o efeito a um 'clip'"

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:180
#, fuzzy
#| msgid "Select the desired region [1]_ to track on the Project Monitor"
msgid "Select the desired region\\ [2]_ to track on the Project Monitor"
msgstr "Seleccione a região desejada [1]_ a seguir no Monitor do Projecto"

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:181
msgid "Choose a tracking algorithm"
msgstr "Escolha um algoritmo de seguimento"

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:182
#, fuzzy
#| msgid "Click on the :guilabel:`Analyse` button"
msgid "Click on the :guilabel:`Analyze` button"
msgstr "Carregue no botão :guilabel:`Analisar`."

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:191
msgid "Options menu"
msgstr "Menu de opções"

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:193
msgid ""
"When the analysis is done you can export the keyframes to the clipboard by "
"clicking on |application-menu| and choose :guilabel:`Copy all keyframes to "
"clipboard`. See also :ref:`Exchanging keyframes <effects-"
"exchange_keyframes>`."
msgstr ""
"Quando a análise terminar, poderá exportar as imagens-chave para a área de "
"transferência, carregando no |application-menu| e escolhendo a opção :"
"menuselection:`Copiar todas as imagens-chave para a área de transferência`. "
"Veja como: :ref:`Trocar as imagens-chave "
"<exchange_keyframes_across_effectsA`."

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:199
#, fuzzy
#| msgid "Tracking algorithms"
msgid "_`Tracking algorithms`"
msgstr "Algoritmos de seguimento"

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:202
msgid "**Kernelized Correlation Filters**"
msgstr "**Filtros de Correlação de Núcleos**"

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:204
msgid ""
"**Pros:** Accuracy and speed are both better than MIL and it reports "
"tracking failure better than MIL."
msgstr ""
"**Vantagens:** A precisão e a velocidade são ambas melhores que o MIL e "
"comunica melhor os problemas de seguimento que o MIL."

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:206
msgid "**Cons:** Does not recover from full occlusion."
msgstr "**Desvantagens:** Não recupera de uma ocultação total."

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:210
msgid "**Channel and Spatial Reliability Tracking**."
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:212
msgid ""
"In the Discriminative Correlation Filter with Channel and Spatial "
"Reliability (DCF-CSR), we use the spatial reliability map for adjusting the "
"filter support to the part of the selected region from the frame for "
"tracking. This ensures enlarging and localization of the selected region and "
"improved tracking of the non-rectangular regions or objects. It uses only 2 "
"standard features (HoGs and Colornames). It also operates at a comparatively "
"lower fps (25 fps) but gives higher accuracy for object tracking."
msgstr ""
"No filtro Discriminative Correlation Filter with Channel and Spatial "
"Reliability (DCF-CSR), usamos o mapa de fiabilidade espacial para ajustar o "
"suporte dos filtros para as partes da região seleccionada na imagem para o "
"seguimento. Isto garante o aumento e a localização da região seleccionada e "
"a melhoria no seguimento das regiões ou objectos não-rectangulares. Só usa 2 "
"funcionalidades-padrão (HoGs e Colornames). Também funciona com uma taxa de "
"IPS relativamente menor (25 IPS), mas gera uma maior precisão no seguimento "
"dos objectos."

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:216
msgid "**Minimum Output Sum of Squared Error**"
msgstr "**Resultado Mínimo da Soma dos Erros ao Quadrado**"

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:218
msgid ""
"MOSSE uses an adaptive correlation for object tracking which produces stable "
"correlation filters when initialized using a single frame. MOSSE tracker is "
"robust to variations in lighting, scale, pose, and non-rigid deformations. "
"It also detects occlusion based upon the peak-to-sidelobe ratio, which "
"enables the tracker to pause and resume where it left off when the object "
"reappears. MOSSE tracker also operates at a higher fps (450 fps and even "
"more)."
msgstr ""
"O MOSSE usa uma correlação adaptativa para o seguimento de objectos que "
"produz filtros de correlação estáveis quando inicializado com uma única "
"imagem. O algoritmo MOSSE é robusto face às variações de iluminação, escala, "
"poses e deformações não-rígidas. Também detecta ocultações com base na "
"relação do pico-para-a-face-lateral, o que permite ao sistema de seguimento "
"pausar e retomar onde ficou quando o objecto reaparecer. O seguimento com o "
"MOSSE também funciona com uma taxa de IPS maior (450 ips ou mais ainda)."

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:220
msgid "**Pros:** It is as accurate as other complex trackers and much faster."
msgstr ""
"**Vantagens:** É tão preciso como os outros algoritmos complexos e é muito "
"mais rápido."

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:222
msgid ""
"**Cons:** On a performance scale, it lags behind the deep learning based "
"trackers."
msgstr ""
"**Desvantagens:** Numa escala de performance, fica atrás dos algoritmos de "
"seguimento baseados na aprendizagem."

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:226
msgid "**Multiple Instance Learning**"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:228
msgid ""
"**Pros:** The performance is pretty good. It does a reasonable job under "
"partial occlusion."
msgstr ""
"**Vantagens:** A performance é bastante boa. Faz um trabalho razoável no "
"caso de ocultações parciais."

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:230
msgid ""
"**Cons:** Tracking failure is not reported reliably. Does not recover from "
"full occlusion."
msgstr ""
"**Desvantagens:** Uma falha no seguimento não é comunicada de forma fiável. "
"Não recupera de uma ocultação total."

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:233
msgid ""
"**Pros:** Excellent tracking failure reporting. Works very well when the "
"motion is predictable and there is no occlusion."
msgstr ""
"**Vantagens:** Comunicação excelente de falhas no seguimento. Funciona muito "
"bem quando o movimento é previsível é não existe ocultação."

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:235
msgid "**Cons:** Fails under large motion."
msgstr "**Desvantagens:** Falha com grandes movimentos."

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:239
msgid ""
"The |DaSiamRPN| visual tracking algorithm relies on deep-learning models to "
"provide extremely accurate results."
msgstr ""
"O algoritmo de seguimento visual |DaSiamRPN| baseia-se e algoritmos de 'deep "
"learning' para oferecer resultados extremamente precisos."

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:241
msgid "In order to use the DaSiam algorithm you need to download the AI models"
msgstr ""
"Para poder usar o algoritmo DaSiam, terá de descarregar os modelos de IA"

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:243
#, fuzzy
#| msgid ""
#| ":download:`https://files.kde.org/kdenlive/motion-tracker/DaSiamRPN/"
#| "dasiamrpn_kernel_cls1.onnx`"
msgid ""
":download:`dasiamrpn_kernel_cls1.onnx<https://files.kde.org/kdenlive/motion-"
"tracker/DaSiamRPN/dasiamrpn_kernel_cls1.onnx>`"
msgstr ""
":download:`https://files.kde.org/kdenlive/motion-tracker/DaSiamRPN/"
"dasiamrpn_kernel_cls1.onnx`"

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:245
#, fuzzy
#| msgid ""
#| ":download:`https://files.kde.org/kdenlive/motion-tracker/DaSiamRPN/"
#| "dasiamrpn_kernel_r1.onnx`"
msgid ""
":download:`dasiamrpn_kernel_r1.onnx<https://files.kde.org/kdenlive/motion-"
"tracker/DaSiamRPN/dasiamrpn_kernel_r1.onnx>`"
msgstr ""
":download:`https://files.kde.org/kdenlive/motion-tracker/DaSiamRPN/"
"dasiamrpn_kernel_r1.onnx`"

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:247
#, fuzzy
#| msgid ""
#| ":download:`https://files.kde.org/kdenlive/motion-tracker/DaSiamRPN/"
#| "dasiamrpn_model.onnx`"
msgid ""
":download:`dasiamrpn_model.onnx<https://files.kde.org/kdenlive/motion-"
"tracker/DaSiamRPN/dasiamrpn_model.onnx>`"
msgstr ""
":download:`https://files.kde.org/kdenlive/motion-tracker/DaSiamRPN/"
"dasiamrpn_model.onnx`"

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:249
#, fuzzy
#| msgid "and place them in:"
msgid "and place them in `folder for models`_"
msgstr "e colocá-los em:"

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:255
#, fuzzy
#| msgid ""
#| "In order to use the DaSiam algorithm you need to download the AI models"
msgid ""
"In order to use the Nano algorithm you need to download the AI models (model "
"size about 1.9 MB)"
msgstr ""
"Para poder usar o algoritmo DaSiam, terá de descarregar os modelos de IA"

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:257
#, fuzzy
#| msgid ""
#| ":download:`https://files.kde.org/kdenlive/motion-tracker/DaSiamRPN/"
#| "dasiamrpn_kernel_cls1.onnx`"
msgid ""
":download:`nanotrack_backbone_sim.onnx<https://files.kde.org/kdenlive/motion-"
"tracker/Nano/nanotrack_backbone_sim.onnx>`"
msgstr ""
":download:`https://files.kde.org/kdenlive/motion-tracker/DaSiamRPN/"
"dasiamrpn_kernel_cls1.onnx`"

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:259
#, fuzzy
#| msgid ""
#| ":download:`https://files.kde.org/kdenlive/motion-tracker/DaSiamRPN/"
#| "dasiamrpn_model.onnx`"
msgid ""
":download:`nanotrack_head_sim.onnx<https://files.kde.org/kdenlive/motion-"
"tracker/Nano/nanotrack_head_sim.onnx>`"
msgstr ""
":download:`https://files.kde.org/kdenlive/motion-tracker/DaSiamRPN/"
"dasiamrpn_model.onnx`"

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:261
msgid "and place them in the `folder for models`_"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:267
msgid "_`Folder for models`"
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:270
msgid ":file:`$HOME/.local/share/kdenlive/opencvmodels`"
msgstr ":file:`$HOME/.local/share/kdenlive/opencvmodels`"

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:273
msgid ":file:`$HOME/.var/app/org.kde.kdenlive/data/kdenlive/opencvmodels`"
msgstr ":file:`$HOME/.var/app/org.kde.kdenlive/data/kdenlive/opencvmodels`"

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:276
#, fuzzy
#| msgid ":file:`$HOME/.local/share/kdenlive/opencvmodels`"
msgid ":file:`$HOME/Library/Application Support/kdenlive/opencvmodels`"
msgstr ":file:`$HOME/.local/share/kdenlive/opencvmodels`"

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:279
msgid ":file:`%AppData%/kdenlive/opencvmodels`"
msgstr ":file:`%AppData%/kdenlive/opencvmodels`"

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:281
msgid ""
"Press :kbd:`Win+R` (:kbd:`Windows` key and :kbd:`R` key simultaneously) and "
"copy **%AppData%/kdenlive/**. Then create the folder `opencvmodels`"
msgstr ""
"Carregue em :kbd:`Win+R` (tecla do :kbd:`Windows` e :kbd:`R` em simultâneo) "
"e copie o **%AppData%/kdenlive/**. Depois crie a pasta `opencvmodels`"

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:284
#, fuzzy
#| msgid "**Windows**"
msgid "Windows Only!"
msgstr "**Windows**"

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:286
msgid ""
"You may get an error of ``mlt_repository_init: failed to dlopen C:\\Program "
"Files\\kdenlive\\lib\\mlt/libmltjack.dll`` or ``animation initialized "
"FAILED`` followed by many lines of ``Current Frame: <f>, percentage: <p>``."
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:288
msgid ""
"In this case it is recommended to delete all :file:`kdenlive` folders in :"
"file:`C:\\\\Program Files\\\\`, :file:`%AppData%\\\\Roaming\\\\`, and :file:`"
"%AppData%\\\\Local\\\\`, and then do a new install of Kdenlive."
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:296
msgid ""
"If you want to build Kdenlive yourself you need to build MLT with OpenCV "
"support. See here for |more_info|."
msgstr ""

#: ../../effects_and_filters/video_effects/alpha_mask_keying/motion_tracker.rst:298
#, fuzzy
#| msgid ""
#| "If you see just a red rectangle in the Project Monitor but cannot move or "
#| "size it enable Edit Mode by clicking on the |edit-mode| icon in the "
#| "Project Monitor toolbar"
msgid ""
"If you see just a red rectangle in the Project Monitor but cannot move or "
"size it, enable Edit Mode by clicking on the |edit-mode| icon in the Project "
"Monitor toolbar"
msgstr ""
"Se vir apenas um rectângulo vermelho no Monitor do Projecto, mas não "
"conseguir movê-lo ou dimensioná-lo, active o Modo de Edição ao carregar no "
"ícone |edit-mode| na barra do Monitor do Projecto"

#~ msgid "Motion Tracker effect panel"
#~ msgstr "Painel do efeito de Seguimento do Movimento"

#~ msgid ""
#~ "Select a color that makes the shape easier to see in the Project Monitor. "
#~ "Has no effect on the tracking."
#~ msgstr ""
#~ "Seleccione uma cor que torne a forma mais fácil de ver no Monitor do "
#~ "Projecto. Não faz efeito no acompanhamento."

#~ msgid ""
#~ "Four blur types are available: Median blur, Gaussian blur, Pixelate, "
#~ "Opaque fill"
#~ msgstr ""
#~ "Estão disponíveis quatro tipos de borrões: borrão da mediana, borrão "
#~ "gaussiano, pixelização, preenchimento opaco"

#~ msgid "**Notes**"
#~ msgstr "**Notas**"
