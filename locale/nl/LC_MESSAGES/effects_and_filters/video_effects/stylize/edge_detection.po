# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# SPDX-FileCopyrightText: 2023 Freek de Kruijf <freekdekruijf@kde.nl>
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-08-21 01:53+0000\n"
"PO-Revision-Date: 2023-12-18 11:38+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.08.4\n"

#: ../../effects_and_filters/video_effects/stylize/edge_detection.rst:0
msgid "**Status**"
msgstr "**Status**"

#: ../../effects_and_filters/video_effects/stylize/edge_detection.rst:0
msgid "**Keyframes**"
msgstr "**Keyframes**"

#: ../../effects_and_filters/video_effects/stylize/edge_detection.rst:0
msgid "**Source library**"
msgstr "**Bronnenbibliotheek**"

#: ../../effects_and_filters/video_effects/stylize/edge_detection.rst:0
msgid "**Source filter**"
msgstr "**Filter op de bron**"

#: ../../effects_and_filters/video_effects/stylize/edge_detection.rst:0
msgid "**Available**"
msgstr "**Beschikbaar**"

#: ../../effects_and_filters/video_effects/stylize/edge_detection.rst:0
msgid "**On Master only**"
msgstr "**Alleen op master**"

#: ../../effects_and_filters/video_effects/stylize/edge_detection.rst:0
msgid "**Known bugs**"
msgstr "**Bekende bugs**"

#: ../../effects_and_filters/video_effects/stylize/edge_detection.rst:1
msgid "Kdenlive Video Effects - Edge Detection"
msgstr "Video-effecten van Kdenlive - randdetectie"

#: ../../effects_and_filters/video_effects/stylize/edge_detection.rst:1
msgid ""
"KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video "
"effects, stylize, edge detection"
msgstr ""
"KDE, Kdenlive, videobewerker, hulp, leren, gemakkelijk, effecten, filter, "
"video-effecten, stileren, randdetectie"

#: ../../effects_and_filters/video_effects/stylize/edge_detection.rst:19
msgid "Edge Detection"
msgstr "Randdetectie"

#: ../../effects_and_filters/video_effects/stylize/edge_detection.rst:30
msgid "Maintained"
msgstr "Onderhouden"

#: ../../effects_and_filters/video_effects/stylize/edge_detection.rst:32
#: ../../effects_and_filters/video_effects/stylize/edge_detection.rst:40
#: ../../effects_and_filters/video_effects/stylize/edge_detection.rst:42
msgid "No"
msgstr "Nee"

#: ../../effects_and_filters/video_effects/stylize/edge_detection.rst:34
msgid "avfilter"
msgstr "avfilter"

#: ../../effects_and_filters/video_effects/stylize/edge_detection.rst:36
msgid "edgedetect"
msgstr "edgedetect"

#: ../../effects_and_filters/video_effects/stylize/edge_detection.rst:38
msgid "|linux| |appimage| |windows| |apple|"
msgstr "|linux| |appimage| |windows| |apple|"

#: ../../effects_and_filters/video_effects/stylize/edge_detection.rst:48
#: ../../effects_and_filters/video_effects/stylize/edge_detection.rst:62
msgid "Description"
msgstr "Beschrijving"

#: ../../effects_and_filters/video_effects/stylize/edge_detection.rst:49
msgid ""
"This effect/filter detects and draws edges using the Canny edge detection "
"algorithm\\ [1]_."
msgstr ""
"Dit effect/filter detecteert en tekent randen met gebruik van het Canny "
"randdetectiealgoritme\\ [1]_."

#: ../../effects_and_filters/video_effects/stylize/edge_detection.rst:53
msgid "Parameters"
msgstr "Parameters"

#: ../../effects_and_filters/video_effects/stylize/edge_detection.rst:60
msgid "Parameter"
msgstr "Parameter"

#: ../../effects_and_filters/video_effects/stylize/edge_detection.rst:61
msgid "Value"
msgstr "Waarde"

#: ../../effects_and_filters/video_effects/stylize/edge_detection.rst:63
msgid "Low / High threshold"
msgstr "Lage / hoge drempel"

#: ../../effects_and_filters/video_effects/stylize/edge_detection.rst:64
msgid "Float"
msgstr "Drijvende komma"

#: ../../effects_and_filters/video_effects/stylize/edge_detection.rst:65
msgid ""
"Set low and high threshold values used by the algorithm. The :guilabel:`High "
"threshold` selects the \"strong\" edge pixels, which are then connected "
"through 8-connectivity with the \"weak\" edge pixels selected by the :"
"guilabel:`Low threshold`. Range is from 0.000 to 1.000 with :guilabel:`Low "
"threshold` being lower or equal to :guilabel:`High threshold`."
msgstr ""
"Waarden van lage en hoge drempel instellen gebruikt door het algoritme. De :"
"guilabel:`Hoge drempel` selecteert de \"sterke\" randpixels, die dan worden "
"verbonden via 8-connectiviteit met de \"zwakke\" randpixels geselecteerd "
"door de :guilabel:`Lage drempel`. Reeks is van 0,000 tot 1,000 met :guilabel:"
"`Lage drempel` lager of gelijk aan :guilabel:`Hoge drempel`."

#: ../../effects_and_filters/video_effects/stylize/edge_detection.rst:66
msgid "Modes"
msgstr "Modi"

#: ../../effects_and_filters/video_effects/stylize/edge_detection.rst:67
#: ../../effects_and_filters/video_effects/stylize/edge_detection.rst:70
msgid "Selection"
msgstr "Selectie"

#: ../../effects_and_filters/video_effects/stylize/edge_detection.rst:68
msgid "Define the drawing mode"
msgstr "De tekenmodus definiëren"

#: ../../effects_and_filters/video_effects/stylize/edge_detection.rst:69
msgid "Planes"
msgstr "Vlakken"

#: ../../effects_and_filters/video_effects/stylize/edge_detection.rst:71
msgid "Select the :term:`planes<plane>` for filtering"
msgstr "De :term:`vlakken<plane>` voor filtering selecteren"

#: ../../effects_and_filters/video_effects/stylize/edge_detection.rst:73
msgid "The following selection items are available:"
msgstr "De volgende selectie-items zijn beschikbaar:"

#: ../../effects_and_filters/video_effects/stylize/edge_detection.rst:75
msgid ":guilabel:`Modes`"
msgstr ":guilabel:`Modi`"

#: ../../effects_and_filters/video_effects/stylize/edge_detection.rst:82
msgid "Wires"
msgstr "Draden"

#: ../../effects_and_filters/video_effects/stylize/edge_detection.rst:83
msgid "Draw white/gray wires on black background (default)"
msgstr "Witte/grijze draden op zwarte achtergrond tekenen (standaard)"

#: ../../effects_and_filters/video_effects/stylize/edge_detection.rst:84
msgid "Colormix"
msgstr "Kleurenmix"

#: ../../effects_and_filters/video_effects/stylize/edge_detection.rst:85
msgid "Mix the colors to create a paint/cartoon effect"
msgstr "De kleuren mixen om een schilder/cartoon-effect te maken"

#: ../../effects_and_filters/video_effects/stylize/edge_detection.rst:86
msgid "Canny"
msgstr "Canny"

#: ../../effects_and_filters/video_effects/stylize/edge_detection.rst:87
msgid "Applies Canny edge detector on all selected :term:`planes<plane>`"
msgstr ""
"De randdetectie Canny toepassen op alle geselecteerde :term:`vlakken<plane>`"

#: ../../effects_and_filters/video_effects/stylize/edge_detection.rst:89
msgid ":guilabel:`Planes`"
msgstr ":guilabel:`Vlakken`"

#: ../../effects_and_filters/video_effects/stylize/edge_detection.rst:96
msgid "None"
msgstr "Geen"

#: ../../effects_and_filters/video_effects/stylize/edge_detection.rst:98
msgid "Y/U/V"
msgstr "Y/U/V"

#: ../../effects_and_filters/video_effects/stylize/edge_detection.rst:99
msgid "Also combinations of the planes are possible. Default is **YUV**."
msgstr "Ook combinaties van de vlakken zijn mogelijk. Standaard is **YUV**."

#: ../../effects_and_filters/video_effects/stylize/edge_detection.rst:100
msgid "Alpha"
msgstr "Alfa"

#: ../../effects_and_filters/video_effects/stylize/edge_detection.rst:106
msgid ""
"For more details refer to the |canny_edge_detector| article in Wikipedia."
msgstr ""
"Voor verdere details, zie het artikel |canny_edge_detector| in Wikipedia."

#~ msgid ""
#~ "Do your first steps with Kdenlive video editor, using edge detection "
#~ "effect"
#~ msgstr ""
#~ "Zet uw eerste stappen met de Kdenlive videobewerker, met gebruik van het "
#~ "effect randdetectie"

#~ msgid "The effect does not have keyframes."
#~ msgstr "Dit effect heeft geen keyframes."

#~ msgid "Edge Detection effect"
#~ msgstr "Effect randdetectie"

#~ msgid ""
#~ "**Modes** - Define the drawing mode: Options are **Wires** (default; draw "
#~ "white/gray wires on black background), **Colormix** (mix the colors to "
#~ "create a paint/cartoon effect), and **Canny** (applies Canny edge "
#~ "detector on all selected :term:`planes<plane>`)."
#~ msgstr ""
#~ "**Modi** - definieert de tekenmodus: opties zijn **Draden** (standaard; "
#~ "teken wit/grijze draden op zwarte achtergrond), **Kleurenmix** (meng de "
#~ "kleuren om een schilder/cartoon-effect te maken) en **Canny** (past de "
#~ "Canny edge detector toe op alle geselecteerde :term:`vlakken<plane>`)."

#~ msgid ""
#~ "**Planes** - Select the :term:`planes<plane>` for filtering. Options are "
#~ "**None**, **Y/U/V** and combinations (default is **YUV**), and **Alpha**"
#~ msgstr ""
#~ "**Vlakken* - de :term:`vlallen<plane>` voor filtering selecteren. Opties "
#~ "zijn **Geen**, **Y/U/V** en combinaties (standaard is **YUV**) en **Alfa**"

#~ msgid "**Notes**"
#~ msgstr "**Notities**"
